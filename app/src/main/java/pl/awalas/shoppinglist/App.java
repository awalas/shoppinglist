package pl.awalas.shoppinglist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import pl.awalas.shoppinglist.di.component.DaggerAppComponent;

public class App extends Application implements HasActivityInjector {
    
    @SuppressLint("StaticFieldLeak")
    public static Context context;
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
    
    @Override
    public void onCreate() {
        super.onCreate();
        this.initDagger();
        context = getApplicationContext();
    }
    
    private void initDagger() {
        DaggerAppComponent.builder().application(this).build().inject(this);
    }
    
    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }
}