package pl.awalas.shoppinglist.views.fragments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import pl.awalas.shoppinglist.R;
import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.viewmodels.ProductViewModel;

public class ProductFragment extends Fragment {
    
    public static final String UID_KEY = "uid";
    
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @BindView(R.id.product_name)
    EditText product_name;
    @BindView(R.id.product_quantity)
    EditText product_quantity;
    @BindView(R.id.product_save)
    Button product_save;
    @BindView(R.id.product_delete)
    Button product_delete;
    private ProductViewModel viewModel;
    private Product product;
    
    public ProductFragment() {
    }
    
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        
        return view;
    }
    
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.configureDagger();
        this.configureViewModel();
    }
    
    private void configureDagger() {
        AndroidSupportInjection.inject(this);
    }
    
    private void configureViewModel() {
        int productID = getArguments().getInt(UID_KEY, -1);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductViewModel.class);
        viewModel.init(productID);
        viewModel.getProduct().observe(this, product -> updateUI(product));
    }
    
    private void updateUI(@Nullable Product product) {
        this.product = product;
        if (product != null) {
            product_name.setText(product.getName());
            product_quantity.setText(product.getQuantity());
        } else {
            product_delete.setVisibility(View.GONE);
            product_save.setText(R.string.product_add);
        }
    }
    
    @OnClick(R.id.product_save)
    void saveProduct() {
        if (product != null) viewModel.editProduct(getActivity(), product,
                product_name.getText().toString(), product_quantity.getText().toString());
        else viewModel.addProduct(getActivity(), product_name.getText().toString(),
                product_quantity.getText().toString());
    }
    
    @OnClick(R.id.product_delete)
    void deleteProduct() {
        viewModel.deleteProduct(getActivity(), product);
    }
}

