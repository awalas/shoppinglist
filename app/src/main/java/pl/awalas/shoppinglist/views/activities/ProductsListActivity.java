package pl.awalas.shoppinglist.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import pl.awalas.shoppinglist.R;
import pl.awalas.shoppinglist.views.fragments.ProductFragment;
import pl.awalas.shoppinglist.views.fragments.ProductsListFragment;

public class ProductsListActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    
    @BindView(R.id.add_product)
    FloatingActionButton add_product;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);
        ButterKnife.bind(this);
        
        this.configureDagger();
        this.showFragment(savedInstanceState);
    }
    
    private void configureDagger() {
        AndroidInjection.inject(this);
    }
    
    private void showFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            
            ProductsListFragment fragment = new ProductsListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, null).commit();
        }
    }
    
    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
    
    @OnClick(R.id.add_product)
    void addProduct(){
        Intent intent = new Intent(this, ProductActivity.class);
        intent.putExtra(ProductFragment.UID_KEY, -1);
        this.startActivity(intent);
    }
}