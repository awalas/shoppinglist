package pl.awalas.shoppinglist.views.fragments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import pl.awalas.shoppinglist.R;
import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.viewmodels.ProductsListViewModel;
import pl.awalas.shoppinglist.views.adapters.ProductAdapter;

public class ProductsListFragment extends Fragment {
    
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private ProductsListViewModel viewModel;
    
    public ProductsListFragment() {
    }
    
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products_list, container, false);
        ButterKnife.bind(this, view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        
        return view;
    }
    
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.configureDagger();
        this.configureViewModel();
    }
    
    private void configureDagger() {
        AndroidSupportInjection.inject(this);
    }
    
    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductsListViewModel.class);
        viewModel.init();
        viewModel.getProducts().observe(this, this::updateUI);
    }
    
    private void updateUI(@Nullable List<Product> products) {
        if (products != null) {
            RecyclerView.Adapter adapter = new ProductAdapter(getActivity(), products, viewModel);
            recyclerView.setAdapter(adapter);
        }
    }
}
