package pl.awalas.shoppinglist.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import pl.awalas.shoppinglist.R;
import pl.awalas.shoppinglist.views.fragments.ProductFragment;

public class ProductActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        
        Bundle bundle = getIntent().getExtras();
        int productID = bundle.getInt(ProductFragment.UID_KEY, -1);
        
        this.configureDagger();
        this.showFragment(savedInstanceState, productID);
    }
    
    private void configureDagger() {
        AndroidInjection.inject(this);
    }
    
    private void showFragment(Bundle savedInstanceState, int productID) {
        if (savedInstanceState == null) {
            
            ProductFragment fragment = new ProductFragment();
    
            Bundle bundle = new Bundle();
            bundle.putInt(ProductFragment.UID_KEY, productID);
            fragment.setArguments(bundle);
    
    
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, null).commit();
        }
    }
    
    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}