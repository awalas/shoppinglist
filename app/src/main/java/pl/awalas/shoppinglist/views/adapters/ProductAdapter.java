package pl.awalas.shoppinglist.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.awalas.shoppinglist.R;
import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.viewmodels.ProductsListViewModel;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    
    private Context context;
    private List<Product> products;
    private ProductsListViewModel productsListViewModel;
    
    public ProductAdapter(Context context, List<Product> products,
            ProductsListViewModel productsListViewModel) {
        this.context = context;
        this.products = products;
        this.productsListViewModel = productsListViewModel;
    }
    
    @NonNull
    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
            int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);
        return new ProductViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = products.get(position);
        holder.name.setText(product.getName());
        holder.quantity.setText(product.getQuantity());
        holder.checked.setChecked(product.isChecker());
        holder.position = position;
    }
    
    @Override
    public int getItemCount() {
        return products.size();
    }
    
    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checked)
        CheckBox checked;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.quantity)
        TextView quantity;
        int position;
        
        ProductViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
        
        @OnClick(R.id.checked)
        void setChecked() {
            productsListViewModel
                    .checkProduct(products.get(position), !products.get(position).isChecker());
        }
        
        @OnClick(R.id.list_item)
        void clickItem() {
            productsListViewModel.showEditProductActivity(context, products.get(position).getId());
        }
    }
    
}
