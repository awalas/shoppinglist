package pl.awalas.shoppinglist.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.Date;

import javax.inject.Inject;

import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.data.repositories.ProductRepository;

public class ProductViewModel extends ViewModel {
    
    private LiveData<Product> product;
    private ProductRepository productRepository;
    
    @Inject
    ProductViewModel(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    
    public void init(int id) {
        if (product != null) return;
        product = productRepository.getProduct(id);
    }
    
    public LiveData<Product> getProduct() {
        return product;
    }
    
    public void editProduct(Activity context, Product product, String name, String quantity) {
        product.setName(name);
        product.setQuantity(quantity);
        productRepository.editProduct(product);
        context.finish();
    }
    
    public void deleteProduct(Activity context, Product product) {
        productRepository.deleteProduct(product.getId());
        context.finish();
    }
    
    public void addProduct(Activity context, String name, String quantity) {
        productRepository.addProduct(new Product(-1, name, quantity, false, new Date()));
        context.finish();
    }
}
