package pl.awalas.shoppinglist.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import javax.inject.Inject;

import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.data.repositories.ProductRepository;
import pl.awalas.shoppinglist.views.activities.ProductActivity;
import pl.awalas.shoppinglist.views.fragments.ProductFragment;

public class ProductsListViewModel extends ViewModel {
    
    private LiveData<List<Product>> products;
    private ProductRepository productRepository;
    
    @Inject
    ProductsListViewModel(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    
    public void init() {
        if (products != null) return;
        products = productRepository.getProducts();
    }
    
    public void checkProduct(Product product, boolean check){
        product.setChecker(check);
        productRepository.editProduct(product);
    }
    
    public void showEditProductActivity(Context context, int productID){
        Intent intent = new Intent(context, ProductActivity.class);
        intent.putExtra(ProductFragment.UID_KEY, productID);
        context.startActivity(intent);
    }
    
    public LiveData<List<Product>> getProducts() {
        return products;
    }
}
