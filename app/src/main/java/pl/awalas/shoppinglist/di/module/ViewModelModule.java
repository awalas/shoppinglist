package pl.awalas.shoppinglist.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import pl.awalas.shoppinglist.di.key.ViewModelKey;
import pl.awalas.shoppinglist.viewmodels.FactoryViewModel;
import pl.awalas.shoppinglist.viewmodels.ProductViewModel;
import pl.awalas.shoppinglist.viewmodels.ProductsListViewModel;

@Module
public abstract class ViewModelModule {
    
    @Binds
    @IntoMap
    @ViewModelKey(ProductsListViewModel.class)
    abstract ViewModel bindProductsListViewModel(ProductsListViewModel repoViewModel);
    
    @Binds
    @IntoMap
    @ViewModelKey(ProductViewModel.class)
    abstract ViewModel bindProductViewModel(ProductViewModel repoViewModel);
    
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(FactoryViewModel factory);
}
