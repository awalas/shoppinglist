package pl.awalas.shoppinglist.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.awalas.shoppinglist.data.local.ShoppingDatabase;
import pl.awalas.shoppinglist.data.local.dao.ProductDao;
import pl.awalas.shoppinglist.data.remote.ProductWebService;
import pl.awalas.shoppinglist.data.repositories.ProductRepository;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
public class AppModule {
    
    @Provides
    @Singleton
    ShoppingDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application, ShoppingDatabase.class, "ShoppingDatabase.db")
                .build();
    }
    
    @Provides
    @Singleton
    ProductDao provideUserDao(ShoppingDatabase database) { return database.productDao(); }
    
    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }
    
    @Provides
    @Singleton
    ProductRepository provideUserRepository(ProductWebService webservice, ProductDao userDao,
            Executor executor) {
        return new ProductRepository(webservice, userDao, executor);
    }
    
    @Provides
    Gson provideGson() { return new GsonBuilder().setDateFormat("yyyy-MM-dd' 'HH:mm:ss").create(); }
    
    @Provides
    Retrofit provideRetrofit(Gson gson) {
        String BASE_URL = "http://shoppinglist.awalas.pl/";
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL).build();
    }
    
    @Provides
    @Singleton
    ProductWebService provideApiWebservice(Retrofit restAdapter) {
        return restAdapter.create(ProductWebService.class);
    }
}