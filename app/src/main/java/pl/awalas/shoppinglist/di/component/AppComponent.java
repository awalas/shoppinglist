package pl.awalas.shoppinglist.di.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import pl.awalas.shoppinglist.App;
import pl.awalas.shoppinglist.di.module.ActivityModule;
import pl.awalas.shoppinglist.di.module.AppModule;
import pl.awalas.shoppinglist.di.module.FragmentModule;

@Singleton
@Component(modules={ActivityModule.class, FragmentModule.class, AppModule.class})
public interface AppComponent {
    
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }
    
    void inject(App app);
}
