package pl.awalas.shoppinglist.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import pl.awalas.shoppinglist.views.activities.ProductActivity;
import pl.awalas.shoppinglist.views.activities.ProductsListActivity;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract ProductsListActivity contributeProductsListActivity();
    
    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract ProductActivity contributeProductActivity();
}
