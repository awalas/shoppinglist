package pl.awalas.shoppinglist.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import pl.awalas.shoppinglist.views.fragments.ProductFragment;
import pl.awalas.shoppinglist.views.fragments.ProductsListFragment;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract ProductsListFragment contributeProductsListFragment();
    
    @ContributesAndroidInjector
    abstract ProductFragment contributeProductFragment();
}