package pl.awalas.shoppinglist.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity
public class Product {
    
    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private int id;
    
    @SerializedName("name")
    @NonNull
    @Expose
    private String name;
    
    @SerializedName("quantity")
    @NonNull
    @Expose
    private String quantity;
    
    @SerializedName("checker")
    @Expose
    private boolean checker;
    
    private Date lastRefresh;
    
    @Ignore
    public Product() {
    }
    
    public Product(@NonNull int id, @NonNull String name, @NonNull String quantity,
                   boolean checker, Date lastRefresh) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.checker = checker;
        this.lastRefresh = lastRefresh;
    }
    
    @NonNull
    public int getId() {
        return id;
    }
    
    public void setId(@NonNull int id) {
        this.id = id;
    }
    
    @NonNull
    public String getName() {
        return name;
    }
    
    public void setName(@NonNull String name) {
        this.name = name;
    }
    
    @NonNull
    public String getQuantity() {
        return quantity;
    }
    
    public void setQuantity(@NonNull String quantity) {
        this.quantity = quantity;
    }
    
    public Date getLastRefresh() {
        return lastRefresh;
    }
    
    public void setLastRefresh(Date lastRefresh) {
        this.lastRefresh = lastRefresh;
    }
    
    public boolean isChecker() {
        return checker;
    }
    
    public void setChecker(boolean checker) {
        this.checker = checker;
    }
}
