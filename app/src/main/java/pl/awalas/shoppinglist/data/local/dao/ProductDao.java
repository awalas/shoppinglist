package pl.awalas.shoppinglist.data.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.Date;
import java.util.List;

import pl.awalas.shoppinglist.data.entity.Product;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ProductDao {

    @Insert(onConflict = REPLACE)
    void save(Product product);
    
    @Insert(onConflict = REPLACE)
    void save(List<Product> products);

    @Query("SELECT * FROM product WHERE id = :id")
    LiveData<Product> loadProduct(int id);

    @Query("SELECT * FROM product")
    LiveData<List<Product>> loadProducts();

    @Query("SELECT * FROM product WHERE lastRefresh > :lastRefreshMax LIMIT 1")
    Product hasProduct(Date lastRefreshMax);
    
    @Query("DELETE FROM product WHERE id = :id")
    void deleteProduct(int id);
}
