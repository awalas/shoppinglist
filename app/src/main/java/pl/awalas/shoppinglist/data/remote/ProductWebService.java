package pl.awalas.shoppinglist.data.remote;

import com.google.gson.JsonObject;

import java.util.List;

import okhttp3.ResponseBody;
import pl.awalas.shoppinglist.data.entity.Product;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProductWebService {
    
    @GET("/products/")
    Call<List<Product>> getProducts();
    
    @GET("/product/{product}")
    Call<Product> getProduct(@Path("product") String productID);
    
    @DELETE("/product/delete/{product}")
    Call<ResponseBody> deleteProduct(@Path("product") int productID);
    
    @POST("/product/add")
    Call<Product> addProduct(@Body Product product);
    
    @POST("/product/edit")
    Call<Product> updateProduct(@Body Product product);
}
