package pl.awalas.shoppinglist.data.repositories;

import android.arch.lifecycle.LiveData;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.ResponseBody;
import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.data.local.dao.ProductDao;
import pl.awalas.shoppinglist.data.remote.ProductWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class ProductRepository {
    
    private static int FRESH_TIMEOUT_IN_MINUTES = 3;
    
    private final ProductWebService webservice;
    private final ProductDao productDao;
    private final Executor executor;
    
    @Inject
    public ProductRepository(ProductWebService webservice, ProductDao productDao,
            Executor executor) {
        this.webservice = webservice;
        this.productDao = productDao;
        this.executor = executor;
    }
    
    public LiveData<List<Product>> getProducts() {
        refreshProducts();
        return productDao.loadProducts();
    }
    
    private void refreshProducts() {
        executor.execute(() -> {
            boolean productsExist = (productDao.hasProduct(getMaxRefreshTime(new Date())) != null);
            
            if (!productsExist) {
                webservice.getProducts().enqueue(new Callback<List<Product>>() {
                    @Override
                    public void onResponse(Call<List<Product>> call,
                            final Response<List<Product>> response) {
                        executor.execute(() -> {
                            List<Product> products = response.body();
                            for (Product product : products) {
                                product.setLastRefresh(new Date());
                            }
                            productDao.save(products);
                        });
                    }
                    
                    @Override
                    public void onFailure(Call<List<Product>> call, Throwable t) {
                    }
                });
            }
        });
    }
    
    private Date getMaxRefreshTime(Date currentDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MINUTE, -FRESH_TIMEOUT_IN_MINUTES);
        return cal.getTime();
    }
    
    public LiveData<Product> getProduct(int id) {
        refreshProducts();
        return productDao.loadProduct(id);
    }
    
    public void addProduct(Product product) {
        executor.execute(() -> webservice.addProduct(product).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                executor.execute(() -> {
                    Product product = response.body();
                    product.setLastRefresh(new Date());
                    productDao.save(product);
                });
            }
            
            @Override
            public void onFailure(Call<Product> call, Throwable t) {
            
            }
        }));
    }
    
    public void editProduct(Product product) {
        executor.execute(() -> webservice.updateProduct(product).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                executor.execute(() -> {
                    Product product = response.body();
                    product.setLastRefresh(new Date());
                    productDao.save(product);
                });
            }
            
            @Override
            public void onFailure(Call<Product> call, Throwable t) {
            
            }
        }));
    }
    
    public void deleteProduct(int productID) {
        executor.execute(
                () -> webservice.deleteProduct(productID).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                            Response<ResponseBody> response) {
                        executor.execute(() -> {
                            refreshProducts();
                        });
                    }
                    
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    
                    }
                }));
        executor.execute(() -> productDao.deleteProduct(productID));
    }
}
