package pl.awalas.shoppinglist.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import pl.awalas.shoppinglist.data.local.converter.DateConverter;
import pl.awalas.shoppinglist.data.local.dao.ProductDao;
import pl.awalas.shoppinglist.data.entity.Product;

@Database(entities = {Product.class}, version = 1, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class ShoppingDatabase extends RoomDatabase {

    private static volatile ShoppingDatabase INSTANCE;

    public abstract ProductDao productDao();
}
