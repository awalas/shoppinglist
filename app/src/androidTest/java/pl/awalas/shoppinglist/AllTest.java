package pl.awalas.shoppinglist;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import pl.awalas.shoppinglist.data.local.dao.ProductDaoInstrumentedTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ProductDaoInstrumentedTest.class})
public class AllTest {}
