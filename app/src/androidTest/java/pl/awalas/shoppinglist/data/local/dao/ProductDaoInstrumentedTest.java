package pl.awalas.shoppinglist.data.local.dao;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.awalas.shoppinglist.data.entity.Product;
import pl.awalas.shoppinglist.data.local.DatabaseInstrumentedTest;
import pl.awalas.shoppinglist.utils.LiveDataTestUtil;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class ProductDaoInstrumentedTest extends DatabaseInstrumentedTest {
    
    @Test
    public void insertOneAndLoad() throws InterruptedException {
        final Product product = new Product(1, "pomidor", "2 szt", false, new Date());
        db.productDao().save(product);
        
        final Product loaded = LiveDataTestUtil.getValue(db.productDao().loadProduct(1));
        assertThat(loaded.getQuantity(), is("2 szt"));
    }
    
    @Test
    public void insertMultipleAndLoad() throws InterruptedException {
        final Product product1 = new Product(1, "pomidor", "2 szt", false, new Date());
        final Product product2 = new Product(2, "lody", "1 opakowanie", true, new Date());
        final List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        db.productDao().save(products);
        
        final List<Product> loaded = LiveDataTestUtil.getValue(db.productDao().loadProducts());
        assertThat(loaded.size(), is(2));
    }
    
    @Test
    public void editAndLoad() throws InterruptedException {
        insertOneAndLoad();
        Product product = LiveDataTestUtil.getValue(db.productDao().loadProduct(1));
        product.setName("edited name");
        db.productDao().save(product);
        final Product loaded = LiveDataTestUtil.getValue(db.productDao().loadProduct(1));
        assertThat(loaded.getName(), is("edited name"));
    }
    
    @Test
    public void delete() throws InterruptedException {
        insertOneAndLoad();
        Product product = LiveDataTestUtil.getValue(db.productDao().loadProduct(1));
        db.productDao().deleteProduct(product.getId());
        final Product loaded = LiveDataTestUtil.getValue(db.productDao().loadProduct(1));
        assertNull(loaded);
    }
}
