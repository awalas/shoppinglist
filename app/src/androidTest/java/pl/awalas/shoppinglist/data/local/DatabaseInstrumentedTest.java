package pl.awalas.shoppinglist.data.local;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;

import pl.awalas.shoppinglist.data.local.ShoppingDatabase;

import static org.junit.Assert.assertEquals;

abstract public class DatabaseInstrumentedTest {

    protected ShoppingDatabase db;

    @Before
    public void initDb() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                ShoppingDatabase.class).build();
    }

    @After
    public void closeDb() {
        db.close();
    }
}
